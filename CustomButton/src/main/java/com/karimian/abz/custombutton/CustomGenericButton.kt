package com.karimian.abz.custombutton

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.annotation.Nullable
import androidx.constraintlayout.widget.ConstraintLayout

class CustomGenericButton(context: Context, @Nullable attrs: AttributeSet) :
    ConstraintLayout(context, attrs) {


    private var view: View = LayoutInflater.from(context).inflate(R.layout.custom_view, this, true)
    private var imageView: RoundedImageView = view.findViewById(R.id.iv_profileImage)
    private var titleTextView: TextView = view.findViewById(R.id.txt_title)
    private var subtitleTextView: TextView = view.findViewById(R.id.txt_subtitle)
    private var rootLayout :LinearLayout =  view.findViewById(R.id.root_layout)
    private var icon: Bitmap? = null

    /**
     * set on click in constructor
     */
    init {
        rootLayout.setOnClickListener {
          performClick();
      }
    }


    /**
     * button with only title
     */
    fun normalButton(title: String?){
        setTitle(title)
    }

    /**
     * button with title & subtitle
     */
    fun withSubtitleButton(title: String?,subTitle: String?){
        setTitle(title)
        setSubtitle(subTitle)
    }

    /**
     * button with title & subtitle & icon
     */
    fun withSubtitleAndIconButton(title: String?,subTitle: String?,@DrawableRes icon: Int){
        setTitle(title)
        setSubtitle(subTitle)
        setIcon(icon)
    }

    private fun setIcon(@DrawableRes icon: Int) {
        imageView.visibility = View.VISIBLE
        this.icon = BitmapFactory.decodeResource(
            resources,
            icon
        )
        imageView.setImageBitmap(this.icon)
    }

    fun getIcon(): Bitmap? {
        return icon
    }

    private fun setTitle(text: String?) {
        titleTextView.text = text
    }

    fun getTitle(): String {
        return titleTextView.text.toString()
    }

    private fun setSubtitle(text: String?) {
        subtitleTextView.visibility = View.VISIBLE
        subtitleTextView.text = text
    }

    fun getSubtitle(): String {
        return subtitleTextView.text.toString()
    }


}