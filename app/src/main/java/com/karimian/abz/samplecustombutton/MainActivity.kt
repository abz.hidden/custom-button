package com.karimian.abz.samplecustombutton

import android.R
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.karimian.abz.custombutton.CustomGenericButton


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.karimian.abz.samplecustombutton.R.layout.activity_main)

        var customView = findViewById<CustomGenericButton>(com.karimian.abz.samplecustombutton.R.id.btn_test)
    //    customView.normalButton("salam")
     //   customView.withSubtitleButton("salam","aleyk")
        customView.withSubtitleAndIconButton("salam","aleyk",R.drawable.ic_dialog_info)
        customView.setOnClickListener {
            Log.e("tag","clicked!")
        }

    }
}